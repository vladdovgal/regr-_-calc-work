import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# -- 0 Build linear model --
from functions import selective_variance, average_shift, selective_average_binary, \
    selective_average_unary, correlation_coef, determination_coef, check_mu, dar_wat_crit, mu_score, \
    check_auto_correlation, corr_coef_significance, check_t, confidence_interval_edges, variance_estimation_e, \
    variance_estimation_a, variance_estimation_b, calc_t_criteria_for_coef, check_coef_t_criteria, fisher_criteria, \
    check_fisher_criteria

n = 10  # number of observations
precision = 5  # digits after coma
d1 = 1.08
d2 = 1.36
gama = 0.95
m = 1  # number of parameters of the regression equation
t_krit_095 = 2.262  # t( n-m, 0.95) from Student distribution table
t_krit_099 = 3.249  # t( n-m, 0.99) from Student distribution table
f_table = 5.36  # Fisher distribution
X = np.array(range(1, n + 1))
Y = np.array([
    2.9973,
    5.186,
    4.72,
    7.416,
    8.426,
    12.016,
    13.229,
    15.804,
    17.572,
    19.6337
])

xmean = np.mean(X)
ymean = np.mean(Y)

# Create pandas dataframe to store our X and y values
df = pd.DataFrame({
    'X': X,
    'Y': Y
})
# Calculate the terms needed for the numerator and denominator of beta
df['xycov'] = (df['X'] - xmean) * (df['Y'] - ymean)
df['xvar'] = (df['X'] - xmean) ** 2

# Calculate beta and alpha
beta = df['xycov'].sum() / df['xvar'].sum()
alpha = ymean - (beta * xmean)
print(f'alpha = {alpha}')
print(f'beta = {beta}')

print(f"Model: Y = {beta} * x + {alpha}")

ypred = alpha + beta * X
yPred = lambda a, b, x: a + b * x
print(f"ypred: {ypred}")

# Plot regression against actual data
plt.figure(figsize=(12, 6))
plt.plot(X, ypred)  # regression line
plt.plot(X, Y, 'ro')  # scatter plot showing actual data
plt.title('Actual vs Predicted')
plt.xlabel('X')
plt.ylabel('y')

plt.show()

# Selective average (sa)
x_avg = selective_average_unary(X)
y_avg = selective_average_unary(Y)
xy_avg = selective_average_binary(X, Y)

print(f"X_sa: {x_avg}\nY_sa: {y_avg} ;\nXY_sa: {xy_avg} \n")

# Selective dispersion (sd)

x_sd = selective_variance(X)
y_sd = selective_variance(Y)

print(f"X_sD: {x_sd}\nY_sD: {y_sd} ;\n")

# Average shift (as)
x_as = average_shift(X)
y_as = average_shift(Y)
print(f"X_as: {x_as}\nY_as: {y_as}\n")

# Correlation coefficient
r = correlation_coef(X, Y)
print(f"Correlation coefficient: {r}")

# Determination coefficient
det = determination_coef(X, Y)
print(f"Determination coefficient: {det}")

# Heteroskedasticity
mu = mu_score(Y)
print(f'Heteroskedasticity by \'mu\' = {mu}: ', check_mu(mu))

# Check Darbin-Watson criteria
dw_crit = dar_wat_crit(Y, ypred)
print(f"Darbin-Watson criteria: DW = {round(dw_crit, 5)}")
print("\n")

# Check auto correlation
auto_correlation_present = check_auto_correlation(dw_crit, d1, d2)
print(f"Auto correlation: {auto_correlation_present}")
print("\n")

#  Static significance assessment
t = corr_coef_significance(r, n, m)
print(f"t = {t}")
print(f"Correlation coefficient is significan: {check_t(t, t_krit_095)}")
print("\n")

# 6 Convenience interval
r1_095, r2_095 = confidence_interval_edges(r, t_krit_095, n)
r1_099, r2_099 = confidence_interval_edges(r, t_krit_099, n)
print(f"Convenience interval: r є ({r1_095}, {r2_095}), t = {t_krit_095}, P = 0.95")
print(f"Convenience interval: r є ({r1_099}, {r2_099}), t = {t_krit_099}, P = 0.99")
print("\n")

# 5
sigma_e = variance_estimation_e(Y, ypred, n)
sigma_a = variance_estimation_a(sigma_e, X, x_avg, n)
sigma_b = variance_estimation_b(sigma_e, X, x_avg)
print(f"Sigma_e = {sigma_e}")
print(f"Sigma_a = {sigma_a}")
print(f"Sigma_b = {sigma_b}")

t_a = calc_t_criteria_for_coef(alpha, sigma_a)
t_b = calc_t_criteria_for_coef(beta, sigma_b)
print(f"T_a = {t_a}")
print(f"T_b = {t_b}")
print("\n")

t_a_sign = check_coef_t_criteria(t_a, t_krit_095)
t_b_sign = check_coef_t_criteria(t_b, t_krit_095)

print(f"Regression parameter alpha is significant: {t_a_sign}")
print(f"Regression parameter beta is significant: {t_b_sign}")

if t_a_sign:
    print(f"Param alpha is statistically significant with probability {gama}")

if t_b_sign:
    print(f"Param beta is statistically significant with probability {gama}")

# 8
yPredForApril = yPred(alpha, beta, 4 * 1.07)
print(f"Predicted value for apr = {yPredForApril}")

# 7 Fisher criteria
f = fisher_criteria(r, n, m)
print(f"Fisher criteria: {f}")
fisher_criteria_accepted = check_fisher_criteria(f, f_table)
if fisher_criteria_accepted:
    print("Regression model is reliable")
