import numpy as np
from scipy.stats import chi2

k_mu = 2
mu_alpha = .4


def cmb(l, r):
    return l / r


# Selective average
def selective_average_unary(t):
    return t.sum() / len(t)


def selective_average_binary(t, l):
    return np.array([t[i] * l[i] for i in range(0, len(t))]).sum() / len(t)


# Selective variance
def selective_variance(t):
    return np.array([np.power(x, 2) for x in t]).sum() / len(t) - np.power(selective_average_unary(t), 2)


# Average shift
def average_shift(t):
    return np.sqrt(selective_variance(t))


def correlation_coef(t, l):
    return cmb(
        selective_average_binary(t, l) - selective_average_unary(t) * selective_average_unary(l),
        (average_shift(t) * average_shift(l))
    )


def determination_coef(t, l):
    return np.power(correlation_coef(t, l), 2)


def mu_score(y):
    y_r = np.array_split(y, k_mu)
    s_r = [np.var(yr) * len(yr) for yr in y_r]
    s = np.sum(s_r)

    mu_numerator = np.product(
        [cmb(sr, len(yr)) ** (len(yr) / 2) for sr, yr in zip(s_r, y_r)]
    )
    mu_denominator = (s / len(y)) ** (len(y) / 2)
    return -2 * np.log(mu_numerator / mu_denominator)


def check_mu(mu_score):
    return mu_score > chi2.isf(mu_alpha, k_mu - 1)


# e_i = y_i - y(x)_i
def dar_wat_crit(t, t_pred):
    e = np.array([t[i] - t_pred[i] for i in range(0, len(t))])
    e_2 = np.array([np.power(e[i], 2) for i in range(0, len(e))])
    e_i_minus_e_i1 = np.array([
        np.power(
            e[i] - e[i - 1],
            2
        )
        for i in range(1, len(e))
    ])
    sum_e_2 = e_2.sum()
    sum_e_i_minus_e_i1 = e_i_minus_e_i1.sum()

    # Darbin-Watson statistic
    dw_stat = sum_e_i_minus_e_i1 / sum_e_2
    return dw_stat


def check_auto_correlation(dw, d1, d2):
    return not ((d1 < dw) and (d2 < dw) and (dw < 4 - d2))


def corr_coef_significance(r, n, m):
    mu_r = cmb(
        np.sqrt(1 - r ** 2),
        np.sqrt(n - m)
    )
    t = r / mu_r
    return t


def check_t(t, t_student):
    return t > t_student


def confidence_interval_edges(r, t_crit, n):
    dif = t_crit * np.sqrt(
        (1 - r ** 2) / (n - 2)
    )
    return r - dif, r + dif


def variance_estimation_e(y, y_predicted, n):
    numerator = np.array(
        [(y[i] - y_predicted[i]) ** 2 for i in range(0, len(y))]
    ).sum()
    denominator = n - 2
    return numerator / denominator


def variance_estimation_a(sigma_e, x, x_avg, n):
    numerator = np.array(
        [x[i] ** 2 for i in range(0, len(x))]
    ).sum()
    denominator = n * np.array(
        [(x[i] - x_avg) ** 2 for i in range(0, len(x))]
    ).sum()
    return sigma_e * numerator / denominator


def variance_estimation_b(sigma_e, x, x_avg):
    denominator = np.array(
        [(x[i] - x_avg) ** 2 for i in range(0, len(x))]
    ).sum()
    return sigma_e / denominator


def calc_t_criteria_for_coef(coefficient, variance):
    return coefficient / variance


def check_coef_t_criteria(t, t_table):
    return t > t_table


def fisher_criteria(r, n, m):
    return (r ** 2) * (n - m - 1) / ((1 - (r ** 2)) * m)


def check_fisher_criteria(f, f_table):
    return f > f_table
